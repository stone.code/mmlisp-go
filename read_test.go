// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

import (
	"strings"
	"testing"
)

func TestRead(t *testing.T) {
	test_pass := []string{
		"()",
		"(QUOTE A)",
		"(QUOTE (A B C))",
		"(CAR (QUOTE (A B C)))" }
	test_fail := []string{
		"(QUOTE A" }

	for _, v := range test_pass {
		tokenizer := NewTokenizer(strings.NewReader(v))
		obj, err := Read(tokenizer)
		if err!=nil {
			t.Errorf( "Failed: %s: %v, %v, %v", v, obj, err )
		}
	}

	for _, v := range test_fail {
		tokenizer := NewTokenizer(strings.NewReader(v))
		_, err := Read(tokenizer)
		if err==nil {
			t.Errorf( "Failed: %s", v )
		}
	}
}