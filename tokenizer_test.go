// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

import (
	"strings"
	"testing"
)

type tokenizer_test struct {
	input string
	output []string
}

func equal( a, b []string ) bool {
	if len(a)!=len(b) {
		return false
	}

	for i:=0; i<len(a); i++ {
		if a[i]!=b[i] {
			return false
		}
	}

	return true
}

func TestTokenizer(t *testing.T) {
	tests := []tokenizer_test {
		{ "()", []string{"(", ")"} },
		{ " ( ) ", []string{"(", ")"} },
		{ " ( ) \n   \n", []string{"(", ")"} },
		{ " ( atom ) ", []string{"(", "atom", ")"} },
		{ " ( atom \n ) ", []string{"(", "atom", ")"} },
		{ " ( atom \n bomb ) ", []string{"(", "atom", "bomb", ")"} },
		{ " atom \n ", []string{"(", "atom", ")"} },
		{ " atom ", []string{"(", "atom", ")"} },
		{ " atom \n ( bomb ) ", []string{"(", "atom", ")", "(", "bomb", ")"} } }

	for _, v := range tests {
		rr := strings.NewReader(v.input)
		tokenizer := NewTokenizer(rr)
		tokens, err := tokenizer.ReadAllTokens()
		if err!=nil {
			t.Errorf( "Failed with error:  %s:  %v", v.input, err )
		}
		if !equal( v.output, tokens ) {
			t.Errorf( "Token list mismatch:  %s:  %v", v.input, tokens )
		}
	}
}
