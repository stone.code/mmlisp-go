// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

import (
	"strconv"
	"reflect"
)

type Reflect struct {
	reflect.Value
}

// NewReflect creates a new lisp object that wraps a value from Go.
func NewReflect(i interface{}) Reflect {
	return Reflect{ reflect.ValueOf(i) }
}

// NewReflects creates a new lisp object that wraps a function or closure from Go.  The
// resulting object can be called or invoked inside the lisp environment.
//
// This function is equivalane to call NewReflect(value).Func().
func NewReflectFunc(i interface{}) Func {
	return Reflect{ reflect.ValueOf(i) }.Func()
}

// Func creates a new callable lisp object.  The wrapped value must represent a function,
// closure, or method.
func (r Reflect) Func() Func {
	if r.Kind()!=reflect.Func {
		panic("mmlisp.Func called with non-function!")
	}

	return Func(func (args *Cons, _ Environment) (Object,error) {
		args_len := args.Len()
		if args_len<r.Type().NumIn() {
			return nil, ErrNoArguments{"#go-func", args}
		}
		if args_len>r.Type().NumIn() {
			return nil, ErrMoreArguments{"#go-func", args}
		}

		params := make( []reflect.Value, 0, args_len )
		for i:=0; i<args_len; i++ {
			params = append( params, to_value( r.Type().In(i), args.car ) )
			args = args.cdr
		}
		ret := r.Call(params)
		return to_obj(ret), nil
	})
}	
	

func to_value( typ reflect.Type, o Object ) reflect.Value {
	if atom, ok := o.(Atom); ok {
		switch typ.Kind() {
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				if val, err := strconv.ParseInt(string(atom), 0, 64); err==nil {
					return reflect.ValueOf(val)
				}
			case reflect.Float32, reflect.Float64:
				if val, err := strconv.ParseFloat(string(atom), 64); err==nil {
					return reflect.ValueOf(val)
				}
		}
		return reflect.ValueOf( string(atom) )
	}

	if typ.Kind()==reflect.String {
		return reflect.ValueOf( o.String() )
	}

	if val, ok := o.(Reflect); ok {
		return val.Value
	}
	return reflect.ValueOf( o )
}

func to_obj_value( v reflect.Value ) Object {
	if obj, ok := v.Interface().(Object); ok {
		return obj
	}

	return Reflect{v}
}

func to_obj( v []reflect.Value ) Object {
	if len(v)==0 {
		return nil
	}
	if len(v)==1 {
		return to_obj_value( v[0] )
	}
	panic( "Not implemented!" )
}
