// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

import (
	"testing"
)

type eval_test struct {
	input, output string
}

func TestEval(t *testing.T) {
	test_cases := []eval_test{
		{ "(QUOTE A)", "A" },
		{ "(QUOTE (A B C))", "( A B C )" },
		{ "(CAR (QUOTE (A B C)))", "A"},
		{ "(CDR (QUOTE (A B C)))", "( B C )"},
		{ "(CDR (QUOTE (A )))", "()"},
		{ "(CONS (QUOTE A) (QUOTE (B C)))", "( A B C )" },
		{ "(EQUAL (QUOTE A) (QUOTE A))", "#T" }, 
		{ "(EQUAL (CAR (CDR (QUOTE (A B)))) (QUOTE B))", "#T" }, 
		{ "(EQUAL (QUOTE A) (QUOTE B))", "()" },
		{ "(ATOM (QUOTE A))", "#T" },
		{ "(COND ((ATOM (QUOTE A)) (QUOTE B)) ((QUOTE T) (QUOTE C)))", "B" },
		{ "(LAMBDA (X Y) (CONS (CAR X) Y))", "#( X Y )( CONS ( CAR X ) Y )" },
		{ "((LAMBDA (X Y) (CONS (CAR X) Y)) (QUOTE (A B)) (QUOTE (C)))", "( A C )" } }

	for _, v := range test_cases {
		tokenizer := NewStringTokenizer(v.input	)
		obj, err := Read(tokenizer)
		if err!=nil {
			t.Errorf( "Failed: %s: %v, %v, %v", v, obj, err )
		}

		obj, err = env.Eval(obj)
		if err!=nil {
			t.Errorf( "Failed Eval: %s: %v", v.input, err )
		} else {
			if obj==nil {
				obj = (*Cons)(nil)
			}		
	
			if obj.String() != v.output {
				t.Errorf( "Failed comparison:  %s:  %s:  %s", v.input, obj.String(), v.output )
			}
		}
	}
}

func TestEvalWithSet(t *testing.T) {
	test_cases := []eval_test{
		{ "(!SET FF (LAMBDA (X Y) (CONS (CAR X) Y)))", "#T" },
		{ "(FF (QUOTE (A B)) (CDR (QUOTE (C D))))", "( A D )" },
		{ "(!SET XX (QUOTE (A B)))", "#T" },
		{ "(CAR XX)", "A" } }
		
	myenv := env.Clone()
	myenv.AllowSet()

	for _, v := range test_cases {
		tokenizer := NewStringTokenizer(v.input	)
		obj, err := Read(tokenizer)
		if err!=nil {
			t.Errorf( "Failed: %s: %v, %v, %v", v, obj, err )
		}

		obj, err = myenv.Eval(obj)
		if err!=nil {
			t.Errorf( "Failed Eval: %s: %v", v.input, err )
		} else {
			if obj==nil {
				obj = (*Cons)(nil)
			}		

			if obj.String() != v.output {
				t.Errorf( "Failed comparison:  %s:  %s:  %s", v.input, obj.String(), v.output )
			}
		}
	}
}