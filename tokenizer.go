// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

import (
	"io"
	"strings"
	"unicode"
)

type tokenizer_state int8

const (
	bol = tokenizer_state(0)
	inside_normal = tokenizer_state(1)
	inside_special = tokenizer_state(2)
)

// A Tokenizer is used to convert a stream of runes into tokens for the lisp processor.
// It also implements rules for parathesis insertion.
type Tokenizer struct {
	reader io.RuneReader
	last_rune rune
	paren_count uint
	state tokenizer_state
}

// NewTokenizer creates a new tokenizer that reads from a RuneReader.
func NewTokenizer(r io.RuneReader) *Tokenizer {
	return &Tokenizer{ r, 0, 0, bol }
}

// NewStringTokenizer creates a new tokenizer that reads from a string.
func NewStringTokenizer(s string) *Tokenizer {
	return NewTokenizer(strings.NewReader(s))
}

// Next returns the next token from the input.  A token is one of three things:  an 
// opening parenthesis, a closing parenthesis, or a string.  These should be interpreted
// as either the start of a list, the end of a list, or an atom.
//
// The strings returned will never contain white space.
func (t *Tokenizer) Next() (string,error) {
	// There is an easy case.  We frequently push back a closing paran.
	// If this is the case, we know the next token to return.
	if t.last_rune==')' {
		t.last_rune=0
		t.paren_count--
		return ")", nil
	}


	var ch rune
	var err error
	if t.last_rune!=0 {
		ch, t.last_rune = t.last_rune, 0
	} else {
		ch, _, err = t.reader.ReadRune()
		if err!=nil {
			if t.paren_count==0 && t.state==inside_special {
				t.state = bol
				return ")", nil
			}
			return "", err
		}
	}
	for unicode.IsSpace(ch) {
		// Check for automatic closing of paren
		if ch=='\n' && t.paren_count==0 && t.state==inside_special {
			t.state = bol
			return ")", nil
		}
		// Check for normal line ending
		if ch=='\n' && t.paren_count==0 && t.state==inside_normal {
			t.state = bol
		}

		ch, _, err = t.reader.ReadRune()
		if err!=nil {
			if t.paren_count==0 && t.state==inside_special {
				t.state = bol
				return ")", nil
			}
			return "", err
		}
	}

	if ch=='(' {
		t.paren_count++
		if t.state==bol {
			t.state = inside_normal
		}
		return "(", nil
	}
	if ch==')' {
		t.paren_count--
		return ")", nil
	}
	if t.state==bol {
		t.last_rune = ch
		t.state = inside_special
		return "(", nil
	}

	var buffer_ [128]rune
	buffer := buffer_[:0]

	for !unicode.IsSpace(ch) && ch!=')' {
		buffer = append(buffer,ch)
		ch, _, err = t.reader.ReadRune()	
		if err!=nil {
			if err==io.EOF {
				return string(buffer), nil
			}
			return string(buffer), err
		}
	}

	if ch==')' {
		t.last_rune = ch
	}
	return string(buffer), nil
}

// ReadAllToken will read all of the tokens remaining in the input, and return them as
// a slice.
func (t *Tokenizer) ReadAllTokens() ([]string, error) {
	ret := []string{}

	token, err := t.Next()
	for err==nil {
		ret = append( ret, token )
		token, err = t.Next()
	}
	if err==io.EOF {
		return ret, nil
	}
	return ret, err
}
