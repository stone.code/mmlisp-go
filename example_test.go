// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

import (
	"fmt"
	"strconv"
)

func ExampleEval() {
	// Create a tokenizer to turn your string into a seequence of tokens.
	t := NewStringTokenizer( "(CDR (QUOTE (A B C D)))" )
	// Start parsing the tokens to create a lisp object to be evaluated
	sexp, err := Read(t)
	if err!=nil {
		panic( err )
	}

	// Evaluate the script using the default environment.
	obj, err := Eval(sexp)
	if err!=nil {
		panic( err )
	}

	// Let's see the result!
	fmt.Printf( "Result:  %v", obj )

	//Output:
	//Result:  ( B C D )
}

func ExampleNewFuncFromNative() {
	// Prepare a custom environment for our new functions
	myenv := NewEnvironment()
	myenv["square"] = NewReflectFunc( func(d float64) float64 {
		return d*d
	} )
	myenv["to-string"] = NewReflectFunc( func(i interface{}) string {
		if obj, ok := i.(Object); ok {
			return obj.String()
		}
		if f, ok := i.(float64); ok {
			return strconv.FormatFloat( f, 'g', -1, 64 )
		}
		return "!unknown!"
	} )

	// As with the other example, tokenize, parse, and evaluate the script.
	// Note how we are using the custom environment for the call to Eval.
	// Error checking has been omitted for this example.
	t := NewStringTokenizer( "(to-string (square 3.5))" )
	sexp, _ := Read(t)
	obj, _ := myenv.Eval(sexp)

	// Let's see the result!
	fmt.Printf( "Result:  %v", obj )

	//Output:
	//Result:  12.25
}
