// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

import (
	"errors"
	"reflect"
)

const (
	tee = Atom("#T")
)

var (
	env = Environment{
		"QUOTE": Func(fn_quote),
		"CAR": Func(fn_car),
		"CDR": Func(fn_cdr),
		"CONS": Func(fn_cons),
		"EQUAL": Func(fn_equal),
		"ATOM": Func(fn_atom),
		"COND": Func(fn_cond),
		"LAMBDA": Func(fn_lambda) }
	
	ELAMBDA = errors.New("Bad syntax for lambda." )
	ELAMBDACALL = errors.New("The number of arguments in the call to lambda is incorrect." )
)

// Eval executes the s-exp using the default environment.
func Eval(sexp Object) (Object,error) {
	return env.Eval(sexp)
}

func fn_atom(args *Cons, env Environment) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "ATOM", args }
	}
	if args.cdr!=nil {
		return nil, ErrMoreArguments{ "ATOM", args }
	}

	if _, ok := args.car.(Atom); ok {
		return tee, nil
	}
	return nil, nil
}

func fn_car(args *Cons, env Environment) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "CAR", args }
	}
	if args.cdr!=nil {
		return nil, ErrMoreArguments{ "CAR", args }
	}
	if cons, ok := args.car.(*Cons); ok {
		return cons.car, nil
	}

	return nil, &ErrBadArguments{"CAR", args}
}

func fn_cdr(args *Cons, env Environment) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "CDR", args }
	}
	if args.cdr!=nil {
		return nil, ErrMoreArguments{ "CDR", args }
	}
	if cons, ok := args.car.(*Cons); ok {
		return cons.cdr, nil
	}

	return nil, &ErrBadArguments{"CDR", args}
}

func fn_cond(args *Cons, env Environment) (Object,error) {
	for args!=nil {
		if cons, ok := args.car.(*Cons); ok {
			if cons.cdr==nil {
				return nil, &ErrBadArguments{"COND", args}
			}
			if cons.cdr.cdr!=nil {
				return nil, &ErrBadArguments{"COND", args}
			}

			pred, err := env.Eval( cons.car )
			if err!=nil {
				return nil, err
			}
			if pred!=nil {
				return env.Eval( cons.cdr.car )
			}
		} else {
			return nil, &ErrBadArguments{"COND", args}
		}
	}

	return nil, nil
}

func fn_cons(args *Cons, env Environment) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "CONS", args }
	}
	if args.cdr==nil {
		return nil, ErrNoArguments{ "CONS", args }
	}
	if args.cdr.cdr!=nil {
		return nil, ErrMoreArguments{ "CONS", args }
	}

	if cons, ok := args.cdr.car.(*Cons); ok {
		return &Cons{args.car, cons}, nil	
	}

	return nil, &ErrBadArguments{"CONS", args}
}

func fn_equal(args *Cons, env Environment) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "EQUAL", args }
	}
	if args.cdr==nil {
		return nil, ErrNoArguments{ "EQUAL", args }
	}
	if args.cdr.cdr!=nil {
		return nil, ErrMoreArguments{ "EQUAL", args }
	}

	first := args.car
	second := args.cdr.car

	if first==second {
		return tee, nil
	}

	first, err := env.Eval( first )
	if err!= nil {
		return nil, err
	}
	second, err = env.Eval( second )
	if err!=nil {
		return nil, err
	}

	if a1, ok1 := first.(Atom); ok1 {
		if a2, ok2 := second.(Atom); ok2 {
			if a1==a2 {
				return tee, nil
			}
		}
		return nil, nil
	}

	// Could implement more functionality in the future
	return nil, nil
}

func fn_lambda(args *Cons, env Environment) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "LAMBDA", args }
	}
	if args.cdr==nil {
		return nil, ErrNoArguments{ "LAMBDA", args }
	}
	if args.cdr.cdr!=nil {
		return nil, ErrMoreArguments{ "LAMBDA", args }
	}

	params, ok := args.car.(*Cons)
	if !ok {
		return nil, &ErrBadArguments{"LAMBDA", args}
	}
	if !params.All( func(o Object) bool {
		_, ok := o.(Atom); return ok
	} ) {
		return nil, &ErrBadArguments{"LAMBDA", args}
	}

	sexp, ok := args.cdr.car.(*Cons)
	if !ok {
		return nil, &ErrBadArguments{"LAMBDA", args}
	}

	return &Lambda{ params, sexp }, nil
}

func fn_set(args *Cons, env Environment) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "!SET", args }
	}
	if args.cdr==nil {
		return nil, ErrNoArguments{ "!SET", args }
	}
	if args.cdr.cdr!=nil {
		return nil, ErrMoreArguments{ "!SET", args }
	}
	// perhaps possible expansion in the future
	// for now, the first item in the list must be an atom
	atom, ok := args.car.(Atom)
	if !ok {
		return nil, &ErrBadArguments{"!SET", args}
	}

	env[string(atom)] = args.cdr.car
	return tee, nil
}	

func fn_quote(args *Cons, env Environment) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "QUOTE", args }
	}
	if args.cdr!=nil {
		return nil, ErrMoreArguments{ "QUOTE", args }
	}

	return args.car, nil
}

func interleave( names, values *Cons ) Environment {
	ret := make( Environment )

	// All of the items in the list 'names' should be atoms
	// The two lists should have the same lengths
	for names!=nil {
		name, _ := names.car.(Atom)
		ret[string(name)] = values.car

		names = names.cdr
		if values!=nil {
			values = values.cdr
		}
	}

	// Should this be an error conditions
	//if values!=nil {
	//}
	
	return ret
}

func replace_atom(sexp Object, params Environment ) Object {
	if cons, ok := sexp.(*Cons); ok {
		tmp := replace_atom( cons.car, params )
		list := &Cons{ tmp, nil }
		list2 := list

		cons = cons.cdr
		for cons!=nil {
			tmp = replace_atom( cons.car, params )
			list2.cdr = &Cons{ tmp, nil }
			list2 = list2.cdr
			cons = cons.cdr
		}

		return list
	}

	if atom, ok := sexp.(Atom); ok {
		value, ok := params[string(atom)]
		if ok {
			return value
		}
	}

	return sexp
}

func eval_lambda(lambda *Lambda, args *Cons, env Environment) (Object,error) {
	params := interleave( lambda.args, args )
	sexp := replace_atom( lambda.sexp, params )

	return env.Eval( sexp )
}

func eval_reflect_struct(rf Reflect, args *Cons ) (Object,error) {
	if args==nil {
		return nil, ErrNoArguments{ "REFLECT", args }
	}
	if args.cdr!=nil {
		return nil, ErrMoreArguments{ "REFLECT", args }
	}
	
	ret := rf.FieldByName( args.car.String() )
	if ret.IsValid() {
		return Reflect{ret}, nil
	}

	ret = rf.MethodByName( args.car.String() )
	if ret.IsValid() {
		return Reflect{ret}.Func(), nil
	}

	// Nothing to evaluate
	return &Cons{ rf, args }, nil
}

// Eval executes the s-exp using the environment.
func (env Environment) Eval(sexp Object) (Object,error) {
	if sexp==nil {
		return nil, nil
	}

	if cons, ok := sexp.(*Cons); ok {
		// If we have a nil pointer, treat as a nil value
		if cons==nil {
			return nil, nil
		}

		// If we have a list that starts with LAMBDA, treat the list
		// as a lambda expression.  This is a special case, as we don't
		// want to evaluate the items in this list.
		if atom, ok := cons.car.(Atom); ok && atom=="LAMBDA" {
			return fn_lambda( cons.cdr, env )
		}

		tmp, err := env.Eval( cons.car)
		if err!=nil {
			return nil, err
		}
		if tmp==nil {
			return nil, nil
		}
		accum := &Cons{ tmp, nil }
		accum2 := accum

		for cons.cdr!=nil {
			cons = cons.cdr

			tmp, err := env.Eval( cons.car)
			if err!=nil {
				return nil, err
			}

			accum2.cdr = &Cons{ tmp, nil }
			accum2 = accum2.cdr
		}

		if lambda, ok := accum.car.(*Lambda); ok {
			return eval_lambda(lambda, accum.cdr,env)
		}
		if fn, ok := accum.car.(Func); ok {
			obj, err := fn(accum.cdr,env)
			return obj, err
		}
		if rf, ok := accum.car.(Reflect); ok {
			if rf.Kind() == reflect.Ptr {
				rf = Reflect{rf.Elem()}
			}
			if rf.Kind() == reflect.Struct {
				return eval_reflect_struct( rf, accum.cdr )
			}
			// TODO:  Add support for other wrapped types
		}	

		return accum, nil
	}

	if atom, ok := sexp.(Atom); ok {
		obj, ok := env[string(atom)]
		if ok {
			return obj, nil
		}
		return sexp, nil
	}

	return sexp, nil
}
