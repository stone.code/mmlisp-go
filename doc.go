// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This package provides an implementation of a simple variant of lisp.  The goal
// is to provide a scripting language for Go programs that does not depend on any
// external libraries.
//
// If there are no deployment issues, you should also consider the existing 
// wrappers for Lua or JavaScript, which are provide languages that are 
// probably more widely used.
package mmlisp
