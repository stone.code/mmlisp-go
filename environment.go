// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

// An Environment stores variables for a lisp execution context.  This map
// stores references the functions and values that the script can access.  These
// values may either definitions created within the lisp script, or provided by 
// Go.
type Environment map[string]Object

// NewEnvironment create a new default environment.  The environment returned
// will contain references to the basic functions necessary for the lisp
// environment.
//
// If you would like create an empty Environment, use make.
func NewEnvironment() Environment {
	return env.Clone()
}

// Clone creates a copy of the environment.  This is useful in preparation for
// modifying the functions, values or both that will be availble to a script.
func (e Environment) Clone() Environment {
	ret := make( Environment, len(e) )

	for k, v := range e {
		ret[k] = v
	}
	return ret
}

// AllowSet modifies the environment by adding a reference to the function !SET.
// This function can be used inside a lisp script to save functions or other values
// to the environment (i.e. set a variable).
func (e Environment) AllowSet() {
	e["!SET"] = Func(fn_set)
}
