// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

func readTail(t *Tokenizer) (*Cons,error) {
	token, err := t.Next()
	if err!=nil {
		return nil, err
	}

	if token==")" {
		return nil, nil
	}

	if token=="(" {
		first, err := readTail(t)
		if err!=nil {
			return nil, err
		}
		second, err := readTail(t)
		if err!=nil {
			return nil, err
		}
		return &Cons{first,second}, nil
	}

	first := Atom(token)
	second, err := readTail(t)
	if err!=nil {
		return nil, err
	}
	return &Cons{first,second}, nil
}

// Read returns the next lisp object from the list of tokens.
func Read(t *Tokenizer) (Object, error) {
	token, err := t.Next()
	if err!=nil {
		return nil, err
	}
	if token=="(" {
		obj, err := readTail(t)
		if obj==nil {
			return nil, err
		}
		return obj, err
	}
	return Atom(token), nil
}
