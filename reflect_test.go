// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

import (
	"strconv"
	"testing"
)

type reflect_test struct {
	name string
	input interface{}
	output string
}

func TestReflectSimpleValue(t *testing.T) {
	test_cases := []reflect_test{
		{ "i", 12, "<int Value>" },
		{ "f", 12.1, "<float64 Value>" },
		{ "s", "Hello, world!", "Hello, world!" } }

	myenv := env.Clone()
	for _, v := range test_cases {
		myenv[ v.name ] = NewReflect( v.input )
	}

	for _, v := range test_cases {
		tokenizer := NewStringTokenizer( v.name )
		obj, err := Read(tokenizer)
		if err!=nil {
			t.Errorf( "Failed: %s: %v, %v, %v", v, obj, err )
		}

		obj, err = myenv.Eval(obj)
		if err!=nil {
			t.Errorf( "Failed Eval: %s: %v", v.input, err )
		} else {
			if obj==nil {
				obj = (*Cons)(nil)
			}		
	
			output := "( " + v.output + " )"
			if obj.String() !=  output {
				t.Errorf( "Failed comparison:  %v:  %s:  %s", v.input, obj.String(), output )
			}
		}
	}

}

func TestFuncNative(t *testing.T) {
	flag := false

	fn := func () {
		flag = true
	}

	myenv := env.Clone()
	myenv["test-func"] = NewReflectFunc( fn )

	tokenizer := NewStringTokenizer("(test-func)")
	obj, err := Read(tokenizer)
	if err!=nil {
		t.Errorf( "Failed: %s: %v, %v, %v", obj, err )
	}

	obj, err = myenv.Eval(obj)
	if err!=nil {
		t.Errorf( "Failed Eval: %s: %v", err )
	} else {
		if obj==nil {
			obj = (*Cons)(nil)
		}		

		if obj.String() != "()" {
			t.Errorf( "Failed comparison:  %s:  %s", obj.String(), "()" )
		}

		if !flag {
			t.Errorf( "Flag not set!" )
		}
	}
}

func TestFuncNative2(t *testing.T) {
	fn1 := func(s string) float64 {
		val, _ := strconv.ParseFloat(s,64)
		return val
	}
	fn2 := func(d float64) float64 {
		return d*d
	}
	fn3 := func(i interface{}) string {
		if obj, ok := i.(Object); ok {
			return obj.String()
		}
		if f, ok := i.(float64); ok {
			return strconv.FormatFloat( f, 'g', -1, 64 )
		}
		return "!unknown!"
	}

	myenv := env.Clone()
	myenv["to-float"] = NewReflectFunc( fn1 )
	myenv["square"] = NewReflectFunc( fn2 )
	myenv["to-string"] = NewReflectFunc( fn3 )

	tokenizer := NewStringTokenizer("(to-string (square (to-float 3.0)))")
	obj, err := Read(tokenizer)
	if err!=nil {
		t.Errorf( "Failed: %s: %v, %v, %v", obj, err )
	}

	obj, err = myenv.Eval(obj)
	if err!=nil {
		t.Errorf( "Failed Eval: %s: %v", err )
	} else {
		if obj==nil {
			obj = (*Cons)(nil)
		}		

		if obj.String() != "9" {
			t.Errorf( "Failed comparison:  %s:  %s", obj.String(), "()" )
		}
	}

	tokenizer = NewStringTokenizer("(to-string (square 3.5))")
	obj, err = Read(tokenizer)
	if err!=nil {
		t.Errorf( "Failed: %s: %v, %v, %v", obj, err )
	}

	obj, err = myenv.Eval(obj)
	if err!=nil {
		t.Errorf( "Failed Eval: %s: %v", err )
	} else {
		if obj==nil {
			obj = (*Cons)(nil)
		}		

		if obj.String() != "12.25" {
			t.Errorf( "Failed comparison:  %s:  %s", obj.String(), "()" )
		}
	}
}

type Struct struct {
	I int
	F float64
}

func (s Struct) Add(rhs float64) float64 {
	return s.F + rhs
}

func (s Struct) Double() int {
	return s.I*2
}

type reflect_struct_test struct { 
	script, output string 
}

func TestReflectStruct(t *testing.T) {
	myenv := env.Clone()
	myenv["my-struct"] = NewReflect( &Struct{ 2, 2.5 } )
	myenv["to-string"] = NewReflectFunc( func(i interface{}) string {
		if obj, ok := i.(Object); ok {
			return obj.String()
		}
		if f, ok := i.(int); ok {
			return strconv.FormatFloat( float64(f), 'f', -1, 64 )
		}
		if f, ok := i.(float64); ok {
			return strconv.FormatFloat( f, 'g', -1, 64 )
		}
		return "!unknown!"
	} )

	tests := []reflect_struct_test{
		{"(my-struct I)", "<int Value>" },
		{"(to-string (my-struct I))", "2" },
		{"(my-struct F)", "<float64 Value>" },
		{"(to-string (my-struct F))", "2.5" },
		{"(my-struct Add)", "#func" },
		{"((my-struct Add) 0.25)", "<float64 Value>" },
		{"(to-string ((my-struct Add) 0.25))", "2.75" },
		{"((my-struct Double))", "<int Value>" },
		{"(to-string ((my-struct Double)))", "4" } }

	for _, v := range tests {

	tokenizer := NewStringTokenizer( v.script )
	obj, err := Read(tokenizer)
	if err!=nil {
		t.Errorf( "Failed: %s: %v, %v, %v", obj, err )
	}

	obj, err = myenv.Eval(obj)
	if err!=nil {
		t.Errorf( "Failed Eval: %s: %v", err )
	} else {
		if obj==nil {
			obj = (*Cons)(nil)
		}		

		if obj.String() != v.output {
			t.Errorf( "Failed comparison:  %s:  %s", obj.String(), v.output )
		}
	}

	}

}