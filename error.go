// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

// An ErrNoArguments is returned when a lisp script is being evaluated that attempts to 
// make a function call, but there are insufficient arguments.
type ErrNoArguments struct {
	Name	string
	Args	*Cons
}

func (e ErrNoArguments) Error() string {
	return "Function used with bad arguments:  " + e.Name + ": " + e.Args.String()
}

// An ErrMoreArguments is returned when a lisp script is being evaluated that attempts to 
// make a function call, but there are too many arguments.
type ErrMoreArguments struct {
	Name	string
	Args	*Cons
}

func (e ErrMoreArguments) Error() string {
	return "Function used with bad arguments:  " + e.Name + ": " + e.Args.String()
}

// An ErrBadArguments is returned when a lisp script is being evaluated that attempts to 
// make a function call, but the arguments are not in the proper format.
type ErrBadArguments struct {
	Name	string
	Args	*Cons
}

func (e ErrBadArguments) Error() string {
	return "Function used with bad arguments:  " + e.Name + ": " + e.Args.String()
}
