// Copyright (c) 2013 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package mmlisp

// An Object represents a value inside the lisp runtime.
type Object interface {
	String() string
}

// An Atom is a fixed string inside the lisp runtime.  Atoms are used as
// identifiers.
type Atom string

// String converts the Atom back into a string.
func (a Atom) String() string {
	return string(a)
}

// A Cons represents the head of a singly-linked list, and is the fundamental
// data type of lisp processing.
type Cons struct {
	car Object
	cdr *Cons
}

// All returns true if and only if all of the items inside the list meet the
// criteria specified by the function pred.
func (c *Cons) All( pred func (o Object) bool ) bool {
	for c!=nil {
		b := pred(c.car)
		if !b {
			return false
		}
		c = c.cdr
	}
	return true
}

// Len returns the number of items in the list.
func (c *Cons) Len() int {
	ret := 0
	for c!=nil {
		ret++
		c = c.cdr
	}
	return ret
}

// String returns a string representation of the list.
func (c *Cons) String() string {
	if c==nil {
		return "()"
	}

	ret := "("

	for c!=nil {
		if c.car==nil {
			ret += " ()"
		} else {
			ret += " " + c.car.String()
		}
		c = c.cdr
	}
	return ret + " )"
}

// A Func represents a function that is callable from within a lisp script.  These
// functions are provided by Go, and cannot be created inside a lisp script.
type Func func (*Cons,Environment) (Object,error)

// String simply returns the value "#func".
func (f Func) String() string {
	return "#func"
}

// A Lambda represents a function that has been defined within a lisp script.
type Lambda struct {
	args *Cons
	sexp *Cons
}


// String returns a string containing the arguments and definition of the lambda.
func (l *Lambda) String() string {
	return "#" + l.args.String() + l.sexp.String()
}
